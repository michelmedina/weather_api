// se genera una constante para evitar errores en las variables cuando se la pasé
export const SET_CITY = 'SET_CITY'

// se le puede llamar payload o value
export const setCity = payload => ({type: SET_CITY, payload });

//con esto se hace la refactorización para que la logico no quede en la App.js
// también se limpia más el código
