import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import ForecastExtended from './../components/ForecastExtended';

class ForecastExtendedContainer extends Component {
  render() {
          // con esto validamos que la propiedad no venga vacía
    return (
      this.props.city &&
      <ForecastExtended city={this.props.city} />
    );
  }
}
// console.log(this.props.city);
// debugger;
ForecastExtendedContainer.propTypes = {
  city: PropTypes.string.isRequired,
};
// tiene un state como parámetro y está esperando que de este state retornemos un objeto
// que va a ser la parte del state que a nosotros nos interesa
// esta city viene desde ForecastExtended

// const mapStateToProps = state => ({  city: state.city });
//  con destructuring
const mapStateToProps = ({ city }) => ({ city });

export default connect(mapStateToProps, null)(ForecastExtendedContainer);


