import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { setCity} from './../actions';
import LocationList from './../components/LocationList';

class LocationListContainer extends Component {
  handleSelectedLocation = city => {

    this.props.setCity(city);
  };

  render() {
    return (
      <LocationList cities={this.props.cities}
              onSelectedLocation={this.handleSelectedLocation}>
      </LocationList>
    );
  }
}

LocationListContainer.propTypes = {
  setCity: PropTypes.func.isRequired,
  cities: PropTypes.array.isRequired,
};

const mapDispatchToProps = dispatch => (
  {
  // con dispatch le asignams el value
  //con el dispatch llamamos al action creator setCity
  //setCity es el action creator, el cual, nos llega desde connect
  //esto es una propiedad del tipo función que devuelve un objeto
  // no confundir el setCity es una propiedad con el setCity que esó una función
  setCity: value => dispatch(setCity(value)) // esto es lo que retorna lo que está recibiendo el dispatch
  //esta función espera un solo parámetro.
});

export default connect(null, mapDispatchToProps)(LocationListContainer);
