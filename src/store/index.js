import { createStore } from 'redux';
import { city } from './../reducers/city';

const initialState = {
  city: 'Caracas,ven'
};


export const store = createStore(() => city, initialState,
window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());

/*Se pasa un reducer como pa´rametro que debe ser una funcipon (un reducer) */
