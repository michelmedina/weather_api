import { SET_CITY } from './../actions'

// Recuder es una funcion que recibe dos parámetros
// 1-> Estado de la aplicación
// 2-> las acciones que vamos generando
// con esos dos parámetros vamos a retornar un nuevo estado
// se genera un nuevo objeto correspondiente a un nuevo estado
//sino corresponde a una nueva acción va a retornar un estado por defecto.
//Al state se le puede dar un valor si viene nulo
export const city = (state = {}, action) => {
  switch (action.type) {
    case SET_CITY:
    //action.payload o value
    //si existe la porpiedad city se modiica con lo que vienen en la accionn
    //si no hay ningún cambio retorna el estado que tiene
      return { ...state, city: action.payload }
    default:
      return state;
  }
}


